using Store.Helpers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Store.Controllers
{
    public class ProductDepartmentSaleController : ApplicationController
    {
        public List<ProductDepartmentSale> index_by(int department_sale_id)
        {
            var con = db();
            DepartmentSale department_sale = con.departments_sales.Find(department_sale_id);

            return department_sale.products_departments_sales.ToList();
        }

        public ProductDepartmentSale create(String department_sale_id, String product_id, String quantity)
        {
            var con = db();
            ProductDepartmentSale product_department_sale = null;

            using (var dbContextTransaction = con.Database.BeginTransaction())
            {
                try
                {
                    Product product = ProductHelper.search(product_id);
                    int product_quantity = Int32.Parse(quantity);

                    if (product.stock.quantity < product_quantity)
                    {
                        throw new Exception("Out of stock");
                    }

                    DepartmentSale department_sale = DepartmentSaleHelper.search(department_sale_id);

                    List<ProductDepartmentSale> products = con.products_departments_sales.Where(pds => pds.sale_id == department_sale.sale_id &&
                        pds.department_sale_id == department_sale.id &&
                        pds.product_id == product.id).ToList();

                    if (products.Count() > 0)
                    {
                        product_department_sale = products.First();
                    }

                    if (product_department_sale == null || product_department_sale.price != product.price)
                    {
                        product_department_sale = new ProductDepartmentSale();
                        product_department_sale.sale_id = department_sale.sale_id;
                        product_department_sale.department_sale_id = department_sale.id;
                        product_department_sale.product_id = product.id;
                        product_department_sale.quantity = product_quantity;
                        product_department_sale.price = product.price;
                        product_department_sale.amount = UtilHelper.amount(product.price, product_department_sale.quantity);
                        con.products_departments_sales.Add(product_department_sale);
                    }
                    else
                    {
                        product_department_sale.quantity += product_quantity;
                        product_department_sale.amount = UtilHelper.amount(product.price, product_department_sale.quantity);
                    }

                    stock_controller().decrease(product.id, product_quantity);
                    sale_controller().increaseAmount(product_department_sale.sale_id, product_department_sale.amount);
                    department_sale_controller().increaseAmount(product_department_sale.department_sale_id, product_department_sale.amount);

                    con.SaveChanges();

                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                }
            }

            return product_department_sale;
        }

        public bool destroy(int id)
        {
            var con = db();
            int rows = 0;

            using (var dbContextTransaction = con.Database.BeginTransaction())
            {
                try
                {
                    ProductDepartmentSale product_department_sale = con.products_departments_sales.Find(id);

                    sale_controller().decreaseAmount(product_department_sale.sale_id, product_department_sale.amount);
                    department_sale_controller().decreaseAmount(product_department_sale.department_sale_id, product_department_sale.amount);
                    stock_controller().increase(product_department_sale.product_id, product_department_sale.quantity);

                    con.products_departments_sales.Attach(product_department_sale);
                    con.products_departments_sales.Remove(product_department_sale);

                    rows = con.SaveChanges();

                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                }
            }

            return rows > 0;
        }

        private SaleController sale_controller()
        {
            return new SaleController();
        }

        private DepartmentSaleController department_sale_controller()
        {
            return new DepartmentSaleController();
        }

        private StockController stock_controller()
        {
            return new StockController();
        }
    }
}