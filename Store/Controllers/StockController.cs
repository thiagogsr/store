﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Controllers
{
    public class StockController : ApplicationController
    {
        public void increase(int product_id, int quantity)
        {
            var con = db();
            con.stocks.Where(stock => stock.product_id == product_id).First().quantity += quantity;
            con.SaveChanges();
        }

        public void decrease(int product_id, int quantity)
        {
            var con = db();
            con.stocks.Where(stock => stock.product_id == product_id).First().quantity -= quantity;
            con.SaveChanges();
        }
    }
}