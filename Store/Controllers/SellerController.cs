using Store.Helpers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Controllers
{
    public class SellerController : ApplicationController
    {
        public List<Seller> index(String department = null, bool active = false, String education = null)
        {
            var con = db();

            IQueryable<Seller> result = con.sellers.Where(seller => seller.deleted == 0);

            if (department != null)
            {
                int department_id = Int32.Parse(department);
                result = result.Where(seller => seller.department_id == department_id);
            }

            if (active)
            {
                result = result.Where(seller => seller.status == 1);
            }

            if (education != null)
            {
                result = result.Where(seller => seller.education == education);
            }

            return result.ToList();
        }

        public bool create(String name, String rg, String cpf, String education, String admission,
            String status, String department_id)
        {
            var con = db();

            Seller seller = new Seller();
            seller.name = name;
            seller.rg = UtilHelper.onlyNumbers(rg);
            seller.cpf = UtilHelper.onlyNumbers(cpf);
            seller.education = education;
            seller.admission = UtilHelper.parseDate(admission);
            seller.status = UtilHelper.parseByte(status);
            seller.department_id = Int32.Parse(department_id);
            con.sellers.Add(seller);

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public Seller edit(int id)
        {
            var con = db();

            return con.sellers.Find(id);
        }

        public bool update(int id, String name, String rg, String cpf, String education,
            String admission, String status, String department_id)
        {
            var con = db();

            Seller seller = con.sellers.Find(id);
            seller.name = name;
            seller.rg = UtilHelper.onlyNumbers(rg);
            seller.cpf = UtilHelper.onlyNumbers(cpf);
            seller.education = education;
            seller.admission = UtilHelper.parseDate(admission);
            seller.status = UtilHelper.parseByte(status);
            seller.department_id = Int32.Parse(department_id);

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public bool destroy(int id)
        {
            var con = db();

            Seller seller = con.sellers.Find(id);
            seller.deleted = 1;

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public BossDepartment current_boss_department(Seller seller)
        {
            BossDepartment boss_department = department_controller().current_boss(seller.department);
            return boss_department != null && boss_department.boss_id == seller.id ? boss_department : null;
        }

        private DepartmentController department_controller()
        {
            return new DepartmentController();
        }
    }
}