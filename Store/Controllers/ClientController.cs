﻿using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Controllers
{
    public class ClientController : ApplicationController
    {
        public List<Client> index()
        {
            var con = db();

            return con.clients.Where(client => client.deleted == 0).ToList();
        }

        public bool create(String name)
        {
            var con = db();

            Client client = new Client();
            client.name = name;
            con.clients.Add(client);

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public Client edit(int id)
        {
            var con = db();

            return con.clients.Find(id);
        }

        public bool update(int id, String name)
        {
            var con = db();

            Client client = con.clients.Find(id);
            client.name = name;

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public bool destroy(int id)
        {
            var con = db();

            Client client = con.clients.Find(id);
            client.deleted = 1;

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }
    }
}