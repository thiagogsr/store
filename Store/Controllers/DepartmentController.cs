using Store.Helpers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Controllers
{
    public class DepartmentController : ApplicationController
    {
        public List<Department> index(String sale_id = null)
        {
            var con = db();

            IQueryable<Department> result = con.departments.Where(department => department.deleted == 0);

            if (sale_id != null)
            {
                Sale sale = con.sales.Find(Int32.Parse(sale_id));
                List<int> departments_ids = sale.departments_sales
                                                .Select(ds => ds.department_id)
                                                .ToList();
                result = result.Where(deparment => !departments_ids.Contains(deparment.id));
            }

            return result.ToList();
        }

        public bool create(String acronym, String commission)
        {
            var con = db();

            Department department = new Department();
            department.acronym = acronym;
            department.commission = UtilHelper.parseDecimal(commission);
            con.departments.Add(department);

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public Department edit(int id)
        {
            var con = db();

            return con.departments.Find(id);
        }

        public bool update(int id, String acronym, String commission)
        {
            var con = db();

            Department department = con.departments.Find(id);
            department.acronym = acronym;
            department.commission = UtilHelper.parseDecimal(commission);

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public bool destroy(int id)
        {
            var con = db();

            Department department = con.departments.Find(id);
            department.deleted = 1;

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public BossDepartment current_boss(Department department)
        {
            List<BossDepartment> bosses = department.bosses_departments.Where(bd => bd.start_date <= DateTime.Today && bd.end_date >= DateTime.Today).ToList();
            return bosses.Count() > 0 ? bosses.Last() : null;
        }
    }
}