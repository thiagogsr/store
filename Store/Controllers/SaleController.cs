using Store.Helpers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityFramework.Extensions;

namespace Store.Controllers
{
    public class SaleController : ApplicationController
    {
        public List<Sale> index(String seller_id = null, String start_date = null, String end_date = null)
        {
            var con = db();

            IQueryable<Sale> sales = con.sales;

            if (seller_id != null && seller_id != "")
            {
                int seller_filter = Int32.Parse(seller_id);

                sales = sales.Join(con.departments_sales,
                                sale => sale.id,
                                ds => ds.sale_id,
                                (sale, ds) => new { Sale = sale, DepartmentSale = ds })
                            .Where(ds => ds.DepartmentSale.seller_id == seller_filter)
                            .Select(sale => sale.Sale)
                            .Distinct();
            }

            if (start_date != null && start_date != "")
            {
                DateTime start_date_filter = UtilHelper.parseDate(start_date);

                sales = sales.Where(sale => sale.sale_date >= start_date_filter);
            }

            if (end_date != null && end_date != "")
            {
                DateTime end_date_filter = UtilHelper.parseDate(end_date);

                sales = sales.Where(sale => sale.sale_date <= end_date_filter);
            }

            sales = sales.OrderByDescending(sale => sale.sale_date);

            return sales.ToList();
        }

        public int create(String sale_date, String client_id)
        {
            var con = db();

            Sale sale = new Sale();
            sale.sale_date = UtilHelper.parseDate(sale_date);
            sale.client_id = Int32.Parse(client_id);
            con.sales.Add(sale);
            con.SaveChanges();

            return sale.id;
        }

        public Sale edit(int id)
        {
            var con = db();

            return con.sales.Find(id);
        }

        public void increaseAmount(int sale_id, decimal amount)
        {
            var con = db();
            con.sales.Find(sale_id).amount += amount;
            con.SaveChanges();
        }

        public void decreaseAmount(int sale_id, decimal amount)
        {
            var con = db();
            con.sales.Find(sale_id).amount -= amount;
            con.SaveChanges();
        }

        public bool destroy(int id)
        {
            var con = db();
            int rows = 0;

            using (var dbContextTransaction = con.Database.BeginTransaction())
            {
                try
                {
                    destroy_dependencies(id);

                    var sale = new Sale { id = id };
                    con.sales.Attach(sale);
                    con.sales.Remove(sale);
                    rows = con.SaveChanges();

                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                }
            }

            return rows > 0;
        }

        private void destroy_dependencies(int id)
        {
            var con = db();

            con.products_departments_sales.Where(pds => pds.sale_id == id).Delete();
            con.departments_sales.Where(ds => ds.sale_id == id).Delete();
        }
    }
}