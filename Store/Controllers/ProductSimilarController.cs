﻿using Store.Helpers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Controllers
{
    public class ProductSimilarController : ApplicationController
    {
        public List<Product> index(String product_id)
        {
            var con = db();

            return ProductHelper.search(product_id).similars.ToList();
        }

        public bool create(String product_id, String similar_id)
        {
            var con = db();

            Product similar = con.products.Find(Int32.Parse(similar_id));
            con.products.Find(Int32.Parse(product_id)).similars.Add(similar);

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public bool destroy(int product_id, int similar_id)
        {
            var con = db();

            Product similar = con.products.Find(similar_id);
            con.products.Find(product_id).similars.Remove(similar);

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }
    }
}