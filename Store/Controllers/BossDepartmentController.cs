﻿using Store.Helpers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Controllers
{
    public class BossDepartmentController : ApplicationController
    {
        public List<BossDepartment> index(String department_id)
        {
            var con = db();
            int id = Int32.Parse(department_id);

            return con.bosses_departments.Where(bd => bd.department_id == id).OrderBy(bd => bd.start_date).ToList();
        }

        public int create(String department_id, String seller_id, String start_date, String end_date)
        {
            var con = db();

            BossDepartment boss = new BossDepartment();
            boss.department_id = Int32.Parse(department_id);
            boss.boss_id = Int32.Parse(seller_id);
            boss.start_date = UtilHelper.parseDate(start_date);

            if (end_date != "")
            {
                boss.end_date = UtilHelper.parseDate(end_date);
            }

            if (boss.start_date > boss.end_date)
            {
                return 0;
            }

            if (conflict(boss))
            {
                return -1;
            }

            if (invalid_end_date(boss))
            {
                return -2;
            }

            con.bosses_departments.Add(boss);
            con.SaveChanges();

            return boss.department_id;
        }

        public BossDepartment edit(String id)
        {
            var con = db();
            int boss_department_id = Int32.Parse(id);

            return con.bosses_departments.Find(boss_department_id);
        }

        public int update(String id, String seller_id, String start_date, String end_date)
        {
            var con = db();
            int boss_department_id = Int32.Parse(id);

            BossDepartment boss = con.bosses_departments.Find(boss_department_id);

            boss.boss_id = Int32.Parse(seller_id);
            boss.start_date = UtilHelper.parseDate(start_date);
            boss.end_date = null;

            if (end_date != "")
            {
                boss.end_date = UtilHelper.parseDate(end_date);
            }

            if (boss.start_date > boss.end_date)
            {
                return 0;
            }

            if (conflict(boss, boss.id))
            {
                return -1;
            }

            if (invalid_end_date(boss))
            {
                return -2;
            }

            con.SaveChanges();

            return boss.department_id;
        }

        public bool destroy(int id)
        {
            var con = db();

            BossDepartment boss_department = new BossDepartment { id = id };
            con.bosses_departments.Attach(boss_department);
            con.bosses_departments.Remove(boss_department);
            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        private bool invalid_end_date(BossDepartment boss)
        {
            if (boss.end_date == null)
            {
                var con = db();

                return con.bosses_departments.Where(bd => bd.department_id == boss.department_id &&
                    bd.end_date == null).Count() > 0;
            }
            return false;
        }

        private bool conflict(BossDepartment boss, int except_id = 0)
        {
            var con = db();

            return con.bosses_departments.Where(bd => bd.department_id == boss.department_id &&
                (
                    (boss.start_date >= bd.start_date && (boss.start_date <= bd.end_date || bd.end_date == null)) ||
                        (boss.end_date >= bd.start_date && (boss.end_date <= bd.end_date || bd.end_date == null))
                )
                && bd.id != except_id
            ).Count() > 0;
        }
    }
}