using Store.Helpers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityFramework.Extensions;

namespace Store.Controllers
{
    public class DepartmentSaleController : ApplicationController
    {
        public List<DepartmentSale> index_by(int sale_id)
        {
            var con = db();
            Sale sale = con.sales.Find(sale_id);

            return sale.departments_sales.ToList();
        }

        public DepartmentSale create(String sale_id, String department_id, String seller_id)
        {
            var con = db();

            Department department = DepartmentHelper.search(department_id);

            DepartmentSale department_sale = new DepartmentSale();
            department_sale.sale_id = Int32.Parse(sale_id);
            department_sale.department_id = Int32.Parse(department_id);
            department_sale.seller_id = Int32.Parse(seller_id);
            department_sale.commission = department.commission;
            con.departments_sales.Add(department_sale);
            con.SaveChanges();

            return department_sale;
        }

        public DepartmentSale edit(int id)
        {
            var con = db();

            return con.departments_sales.Find(id);
        }

        public void increaseAmount(int department_sale_id, decimal amount)
        {
            var con = db();
            con.departments_sales.Find(department_sale_id).amount += amount;
            con.SaveChanges();
        }

        public void decreaseAmount(int department_sale_id, decimal amount)
        {
            var con = db();
            con.departments_sales.Find(department_sale_id).amount -= amount;
            con.SaveChanges();
        }

        public bool destroy(int id)
        {
            var con = db();
            int rows = 0;

            using (var dbContextTransaction = con.Database.BeginTransaction())
            {
                try
                {
                    destroy_dependencies(id);

                    var department_sale = new DepartmentSale { id = id };
                    con.departments_sales.Attach(department_sale);
                    con.departments_sales.Remove(department_sale);
                    rows = con.SaveChanges();

                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                }
            }

            return rows.Equals(1);
        }

        private void destroy_dependencies(int id)
        {
            db().products_departments_sales.Where(pds => pds.department_sale_id == id).Delete();
        }
    }
}