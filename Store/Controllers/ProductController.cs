using Store.Helpers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Controllers
{
    public class ProductController : ApplicationController
    {
        public List<Product> index(String department_sale = null, String prod = null)
        {
            var con = db();

            IQueryable<Product> result = con.products.Where(product => product.deleted == 0);

            if (department_sale != null)
            {
                int department_sale_id = Int32.Parse(department_sale);
                DepartmentSale ds = con.departments_sales.Find(department_sale_id);
                result = result.Where(product => product.department_id == ds.department_id && product.stock.quantity > 0);
            }

            if (prod != null)
            {
                int id = Int32.Parse(prod);

                Product p = con.products.Find(id);
                List<int> similars_ids = p.similars.Select(s => s.id).ToList();
                similars_ids.Add(id);

                result = result.Where(product => !similars_ids.Contains(product.id))
                    .Where(product => product.product_type_id == p.product_type_id);
            }

            return result.ToList();
        }

        public List<Product> index_by(int department_id)
        {
            var con = db();

            return con.products.Where(product => product.deleted == 0 &&
                product.department_id == department_id).ToList();
        }

        public bool create(String title, String price, String department_id, String product_type_id)
        {
            var con = db();
            int rows = 0;

            using (var dbContextTransaction = con.Database.BeginTransaction())
            {
                try
                {
                    Product product = new Product();
                    product.title = title;
                    product.price = UtilHelper.parseDecimal(price);
                    product.department_id = Int32.Parse(department_id);
                    product.product_type_id = Int32.Parse(product_type_id);
                    con.products.Add(product);

                    Stock stock = new Stock();
                    stock.product = product;
                    stock.quantity = 0;
                    con.stocks.Add(stock);

                    rows = con.SaveChanges();

                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                }
            }

            return rows.Equals(2);
        }

        public Product edit(int id)
        {
            var con = db();

            return con.products.Find(id);
        }

        public bool update(int id, String title, String price, String department_id, String product_type_id)
        {
            var con = db();

            Product product = con.products.Find(id);
            product.title = title;
            product.price = UtilHelper.parseDecimal(price);
            product.department_id = Int32.Parse(department_id);
            product.product_type_id = Int32.Parse(product_type_id);

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public bool destroy(int id)
        {
            var con = db();

            Product product = con.products.Find(id);
            product.deleted = 1;

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }
    }
}