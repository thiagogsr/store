﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Store.Controllers
{
    public class ReportController : ApplicationController
    {
        public List<DepartmentReport> departments()
        {
            var con = db();

            String query = "SELECT d.acronym AS department_acronym, " +
                            "YEAR(s.sale_date) AS year, " +
                            "MONTH(s.sale_date) AS month, " +
                            "SUM(ds.amount) AS department_amount " +
                            "FROM departments_sales ds " +
                            "INNER JOIN sales s ON ds.sale_id = s.id " +
                            "INNER JOIN departments d ON d.id = ds.department_id " +
                            "WHERE d.deleted = 0 " +
                            "GROUP BY d.acronym, YEAR(s.sale_date), MONTH(s.sale_date) " +
                            "ORDER BY year, month, department_acronym;";

            return con.Database.SqlQuery<DepartmentReport>(query).ToList();
        }

        public List<SaleReport> sales()
        {
            var con = db();

            String query = "SELECT YEAR(s.sale_date) AS year, " +
                            "MONTH(s.sale_date) AS month, " +
                            "SUM(s.amount) AS sale_amount, " +
                            "SUM(ds.commission / 100 * ds.amount) AS sale_commission " +
                            "FROM sales s " +
                            "LEFT JOIN departments_sales ds ON s.id = ds.sale_id " +
                            "GROUP BY YEAR(s.sale_date), MONTH(s.sale_date) " +
                            "ORDER BY year, month, sale_amount;";

            return con.Database.SqlQuery<SaleReport>(query).ToList();
        }
    }

    public class SaleReport
    {
        public int year { get; set; }
        public int month { get; set; }
        public decimal sale_amount { get; set; }
        public decimal sale_commission { get; set; }
    }

    public class DepartmentReport
    {
        public string department_acronym { get; set; }
        public int year { get; set; }
        public int month { get; set; }
        public decimal department_amount { get; set; }
    }
}