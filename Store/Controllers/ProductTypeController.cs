using Store.Helpers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Controllers
{
    public class ProductTypeController : ApplicationController
    {
        public List<ProductType> index()
        {
            var con = db();

            return con.products_types.Where(pt => pt.deleted == 0).ToList();
        }

        public bool create(String title, String allowSimilar)
        {
            var con = db();

            ProductType product_type = new ProductType();
            product_type.title = title;
            product_type.allow_similar = UtilHelper.parseByte(allowSimilar);
            con.products_types.Add(product_type);

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public ProductType edit(int id)
        {
            var con = db();

            return con.products_types.Find(id);
        }

        public bool update(int id, String title, String allowSimilar)
        {
            var con = db();

            ProductType product_type = con.products_types.Find(id);
            product_type.title = title;
            product_type.allow_similar = UtilHelper.parseByte(allowSimilar);

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }

        public bool destroy(int id)
        {
            var con = db();

            ProductType product_type = con.products_types.Find(id);
            product_type.deleted = 1;

            int rows = con.SaveChanges();

            return rows.Equals(1);
        }
    }
}