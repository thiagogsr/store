﻿$(document).foundation();

$('.date').mask('99/99/9999');
$('.cpf').mask('999.999.999-99');
$('.price').priceFormat({
    prefix: '',
    centsSeparator: ',',
    thousandsSeparator: ''
});

function ValidatorUpdateDisplay(el) {
    var validator = $(el),
        label = validator.prev('label'),
        errorClass = 'error';

    if (el.isvalid) {
        label.removeClass(errorClass);
        validator.removeClass(errorClass).hide();
    }
    else
    {
        label.addClass(errorClass);
        validator.addClass(errorClass).show();
    }
}