USE [store]
GO
ALTER TABLE [dbo].[stocks] DROP CONSTRAINT [FK_stocks_products]
GO
ALTER TABLE [dbo].[similar_products] DROP CONSTRAINT [FK_similar_products_products1]
GO
ALTER TABLE [dbo].[similar_products] DROP CONSTRAINT [FK_similar_products_products]
GO
ALTER TABLE [dbo].[sellers] DROP CONSTRAINT [FK_sellers_departments]
GO
ALTER TABLE [dbo].[sales] DROP CONSTRAINT [FK_sales_clients]
GO
ALTER TABLE [dbo].[products_departments_sales] DROP CONSTRAINT [FK_products_departments_sales_sales]
GO
ALTER TABLE [dbo].[products_departments_sales] DROP CONSTRAINT [FK_products_departments_sales_products]
GO
ALTER TABLE [dbo].[products_departments_sales] DROP CONSTRAINT [FK_products_departments_sales_departaments_sales]
GO
ALTER TABLE [dbo].[products] DROP CONSTRAINT [FK_products_products_types]
GO
ALTER TABLE [dbo].[products] DROP CONSTRAINT [FK_products_departments]
GO
ALTER TABLE [dbo].[departments_sales] DROP CONSTRAINT [FK_departments_sales_sellers]
GO
ALTER TABLE [dbo].[departments_sales] DROP CONSTRAINT [FK_departments_sales_sales]
GO
ALTER TABLE [dbo].[departments_sales] DROP CONSTRAINT [FK_departments_sales_departments]
GO
ALTER TABLE [dbo].[bosses_departments] DROP CONSTRAINT [FK_bosses_departments_sellers]
GO
ALTER TABLE [dbo].[bosses_departments] DROP CONSTRAINT [FK_bosses_departments_departaments]
GO
/****** Object:  Index [IX_stocks]    Script Date: 08/06/2015 19:43:06 ******/
ALTER TABLE [dbo].[stocks] DROP CONSTRAINT [IX_stocks]
GO
/****** Object:  Index [IX_produtos_similares]    Script Date: 08/06/2015 19:43:06 ******/
ALTER TABLE [dbo].[similar_products] DROP CONSTRAINT [IX_produtos_similares]
GO
/****** Object:  Index [IX_products_products_types]    Script Date: 08/06/2015 19:43:06 ******/
DROP INDEX [IX_products_products_types] ON [dbo].[products]
GO
/****** Object:  Index [IX_products_departaments]    Script Date: 08/06/2015 19:43:06 ******/
DROP INDEX [IX_products_departaments] ON [dbo].[products]
GO
/****** Object:  Index [IX_departments_sales]    Script Date: 08/06/2015 19:43:06 ******/
ALTER TABLE [dbo].[departments_sales] DROP CONSTRAINT [IX_departments_sales]
GO
/****** Object:  Table [dbo].[stocks]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[stocks]
GO
/****** Object:  Table [dbo].[similar_products]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[similar_products]
GO
/****** Object:  Table [dbo].[sellers]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[sellers]
GO
/****** Object:  Table [dbo].[sales]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[sales]
GO
/****** Object:  Table [dbo].[products_types]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[products_types]
GO
/****** Object:  Table [dbo].[products_departments_sales]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[products_departments_sales]
GO
/****** Object:  Table [dbo].[products]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[products]
GO
/****** Object:  Table [dbo].[departments_sales]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[departments_sales]
GO
/****** Object:  Table [dbo].[departments]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[departments]
GO
/****** Object:  Table [dbo].[clients]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[clients]
GO
/****** Object:  Table [dbo].[bosses_departments]    Script Date: 08/06/2015 19:43:06 ******/
DROP TABLE [dbo].[bosses_departments]
GO
USE [master]
GO
/****** Object:  Database [store]    Script Date: 08/06/2015 19:43:06 ******/
DROP DATABASE [store]
GO
/****** Object:  Database [store]    Script Date: 08/06/2015 19:43:06 ******/
CREATE DATABASE [store]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'store', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\store.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'store_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\store_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [store] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [store].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [store] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [store] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [store] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [store] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [store] SET ARITHABORT OFF 
GO
ALTER DATABASE [store] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [store] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [store] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [store] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [store] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [store] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [store] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [store] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [store] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [store] SET  DISABLE_BROKER 
GO
ALTER DATABASE [store] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [store] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [store] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [store] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [store] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [store] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [store] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [store] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [store] SET  MULTI_USER 
GO
ALTER DATABASE [store] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [store] SET DB_CHAINING OFF 
GO
ALTER DATABASE [store] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [store] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [store] SET DELAYED_DURABILITY = DISABLED 
GO
USE [store]
GO
/****** Object:  Table [dbo].[bosses_departments]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bosses_departments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[boss_id] [int] NOT NULL,
	[department_id] [int] NOT NULL,
	[start_date] [date] NOT NULL,
	[end_date] [date] NULL,
 CONSTRAINT [PK_departamentos_chefes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[clients]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[clients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[deleted] [tinyint] NOT NULL CONSTRAINT [DF_clients_deleted]  DEFAULT ((0)),
 CONSTRAINT [PK_clientes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[departments]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[departments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[acronym] [varchar](50) NOT NULL,
	[commission] [numeric](5, 2) NOT NULL,
	[deleted] [tinyint] NOT NULL CONSTRAINT [DF_departments_deleted]  DEFAULT ((0)),
 CONSTRAINT [PK_departamentos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[departments_sales]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[departments_sales](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sale_id] [int] NOT NULL,
	[seller_id] [int] NOT NULL,
	[department_id] [int] NOT NULL,
	[amount] [numeric](10, 2) NOT NULL CONSTRAINT [DF_departments_sales_amount]  DEFAULT ((0)),
	[commission] [numeric](5, 2) NOT NULL,
 CONSTRAINT [PK_vendas_departamentos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[products]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[products](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[department_id] [int] NOT NULL,
	[product_type_id] [int] NOT NULL,
	[price] [numeric](10, 2) NOT NULL,
	[title] [varchar](150) NOT NULL,
	[deleted] [tinyint] NOT NULL CONSTRAINT [DF_products_deleted]  DEFAULT ((0)),
 CONSTRAINT [PK_produtos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[products_departments_sales]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[products_departments_sales](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sale_id] [int] NOT NULL,
	[department_sale_id] [int] NOT NULL,
	[product_id] [int] NOT NULL,
	[price] [numeric](10, 2) NOT NULL,
	[quantity] [int] NOT NULL,
	[amount] [numeric](10, 2) NOT NULL CONSTRAINT [DF_products_departments_sales_amount]  DEFAULT ((0)),
 CONSTRAINT [PK_vendas_departamentos_produtos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[products_types]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[products_types](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[allow_similar] [tinyint] NOT NULL CONSTRAINT [DF_products_types_allow_similar]  DEFAULT ((0)),
	[deleted] [tinyint] NOT NULL CONSTRAINT [DF_products_types_deleted]  DEFAULT ((0)),
 CONSTRAINT [PK_tipos_produto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sales]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sales](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[client_id] [int] NOT NULL,
	[sale_date] [date] NOT NULL,
	[amount] [numeric](10, 2) NOT NULL CONSTRAINT [DF_sales_amount]  DEFAULT ((0)),
 CONSTRAINT [PK_vendas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sellers]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sellers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[rg] [varchar](20) NULL,
	[cpf] [varchar](11) NULL,
	[education] [varchar](20) NOT NULL,
	[admission] [date] NOT NULL,
	[status] [tinyint] NOT NULL,
	[department_id] [int] NOT NULL,
	[deleted] [tinyint] NOT NULL CONSTRAINT [DF_sellers_deleted]  DEFAULT ((0)),
 CONSTRAINT [PK_vendedores] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[similar_products]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[similar_products](
	[product_id] [int] NOT NULL,
	[similar_id] [int] NOT NULL,
 CONSTRAINT [PK_produtos_similares] PRIMARY KEY CLUSTERED 
(
	[similar_id] ASC,
	[product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[stocks]    Script Date: 08/06/2015 19:43:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[stocks](
	[product_id] [int] NOT NULL,
	[quantity] [int] NOT NULL,
 CONSTRAINT [PK_stocks] PRIMARY KEY CLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[clients] ON 

INSERT [dbo].[clients] ([id], [name], [deleted]) VALUES (6, N'Thiago Guimaraes Santa Rosa', 1)
INSERT [dbo].[clients] ([id], [name], [deleted]) VALUES (7, N'Thiago Guimarães Santa Rosa', 0)
INSERT [dbo].[clients] ([id], [name], [deleted]) VALUES (8, N'Wesley', 1)
SET IDENTITY_INSERT [dbo].[clients] OFF
SET IDENTITY_INSERT [dbo].[departments] ON 

INSERT [dbo].[departments] ([id], [acronym], [commission], [deleted]) VALUES (4, N'Cozinha', CAST(10.00 AS Numeric(5, 2)), 0)
INSERT [dbo].[departments] ([id], [acronym], [commission], [deleted]) VALUES (5, N'Eletrônicos', CAST(15.00 AS Numeric(5, 2)), 0)
SET IDENTITY_INSERT [dbo].[departments] OFF
SET IDENTITY_INSERT [dbo].[departments_sales] ON 

INSERT [dbo].[departments_sales] ([id], [sale_id], [seller_id], [department_id], [amount], [commission]) VALUES (18, 7, 4, 5, CAST(0.00 AS Numeric(10, 2)), CAST(15.00 AS Numeric(5, 2)))
INSERT [dbo].[departments_sales] ([id], [sale_id], [seller_id], [department_id], [amount], [commission]) VALUES (19, 7, 3, 4, CAST(0.00 AS Numeric(10, 2)), CAST(10.00 AS Numeric(5, 2)))
INSERT [dbo].[departments_sales] ([id], [sale_id], [seller_id], [department_id], [amount], [commission]) VALUES (20, 8, 4, 4, CAST(84809.00 AS Numeric(10, 2)), CAST(10.00 AS Numeric(5, 2)))
INSERT [dbo].[departments_sales] ([id], [sale_id], [seller_id], [department_id], [amount], [commission]) VALUES (21, 8, 4, 5, CAST(2000.00 AS Numeric(10, 2)), CAST(15.00 AS Numeric(5, 2)))
SET IDENTITY_INSERT [dbo].[departments_sales] OFF
SET IDENTITY_INSERT [dbo].[products] ON 

INSERT [dbo].[products] ([id], [department_id], [product_type_id], [price], [title], [deleted]) VALUES (9, 4, 2, CAST(4000.00 AS Numeric(10, 2)), N'Macbook Pro', 0)
INSERT [dbo].[products] ([id], [department_id], [product_type_id], [price], [title], [deleted]) VALUES (10, 5, 2, CAST(3000.00 AS Numeric(10, 2)), N'Macbook Air', 0)
INSERT [dbo].[products] ([id], [department_id], [product_type_id], [price], [title], [deleted]) VALUES (11, 5, 2, CAST(2000.00 AS Numeric(10, 2)), N'Macbook White', 0)
INSERT [dbo].[products] ([id], [department_id], [product_type_id], [price], [title], [deleted]) VALUES (12, 4, 3, CAST(80.90 AS Numeric(10, 2)), N'Red Label', 0)
SET IDENTITY_INSERT [dbo].[products] OFF
SET IDENTITY_INSERT [dbo].[products_departments_sales] ON 

INSERT [dbo].[products_departments_sales] ([id], [sale_id], [department_sale_id], [product_id], [price], [quantity], [amount]) VALUES (10, 8, 20, 12, CAST(80.90 AS Numeric(10, 2)), 10, CAST(809.00 AS Numeric(10, 2)))
INSERT [dbo].[products_departments_sales] ([id], [sale_id], [department_sale_id], [product_id], [price], [quantity], [amount]) VALUES (11, 8, 20, 9, CAST(4000.00 AS Numeric(10, 2)), 1, CAST(4000.00 AS Numeric(10, 2)))
INSERT [dbo].[products_departments_sales] ([id], [sale_id], [department_sale_id], [product_id], [price], [quantity], [amount]) VALUES (12, 8, 21, 11, CAST(2000.00 AS Numeric(10, 2)), 1, CAST(2000.00 AS Numeric(10, 2)))
INSERT [dbo].[products_departments_sales] ([id], [sale_id], [department_sale_id], [product_id], [price], [quantity], [amount]) VALUES (13, 8, 20, 9, CAST(4000.00 AS Numeric(10, 2)), 20, CAST(80000.00 AS Numeric(10, 2)))
SET IDENTITY_INSERT [dbo].[products_departments_sales] OFF
SET IDENTITY_INSERT [dbo].[products_types] ON 

INSERT [dbo].[products_types] ([id], [title], [allow_similar], [deleted]) VALUES (1, N'Eletroeletronicos', 0, 1)
INSERT [dbo].[products_types] ([id], [title], [allow_similar], [deleted]) VALUES (2, N'Eletroeletronicos', 1, 0)
INSERT [dbo].[products_types] ([id], [title], [allow_similar], [deleted]) VALUES (3, N'Bebidas', 0, 0)
SET IDENTITY_INSERT [dbo].[products_types] OFF
SET IDENTITY_INSERT [dbo].[sales] ON 

INSERT [dbo].[sales] ([id], [client_id], [sale_date], [amount]) VALUES (6, 7, CAST(N'2015-05-22' AS Date), CAST(0.00 AS Numeric(10, 2)))
INSERT [dbo].[sales] ([id], [client_id], [sale_date], [amount]) VALUES (7, 7, CAST(N'2015-05-27' AS Date), CAST(0.00 AS Numeric(10, 2)))
INSERT [dbo].[sales] ([id], [client_id], [sale_date], [amount]) VALUES (8, 7, CAST(N'2015-06-08' AS Date), CAST(86809.00 AS Numeric(10, 2)))
SET IDENTITY_INSERT [dbo].[sales] OFF
SET IDENTITY_INSERT [dbo].[sellers] ON 

INSERT [dbo].[sellers] ([id], [name], [rg], [cpf], [education], [admission], [status], [department_id], [deleted]) VALUES (3, N'Priscilla Meneses Santos', N'31430198', N'04868092502', N'MEDIO', CAST(N'2014-08-15' AS Date), 1, 4, 0)
INSERT [dbo].[sellers] ([id], [name], [rg], [cpf], [education], [admission], [status], [department_id], [deleted]) VALUES (4, N'Maria Jose', N'1234423423', N'23432432423', N'MEDIO', CAST(N'2015-01-01' AS Date), 1, 5, 0)
SET IDENTITY_INSERT [dbo].[sellers] OFF
INSERT [dbo].[similar_products] ([product_id], [similar_id]) VALUES (9, 10)
INSERT [dbo].[similar_products] ([product_id], [similar_id]) VALUES (9, 11)
INSERT [dbo].[stocks] ([product_id], [quantity]) VALUES (9, 0)
INSERT [dbo].[stocks] ([product_id], [quantity]) VALUES (10, 10)
INSERT [dbo].[stocks] ([product_id], [quantity]) VALUES (11, 9)
INSERT [dbo].[stocks] ([product_id], [quantity]) VALUES (12, 0)
/****** Object:  Index [IX_departments_sales]    Script Date: 08/06/2015 19:43:07 ******/
ALTER TABLE [dbo].[departments_sales] ADD  CONSTRAINT [IX_departments_sales] UNIQUE NONCLUSTERED 
(
	[department_id] ASC,
	[sale_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_products_departaments]    Script Date: 08/06/2015 19:43:07 ******/
CREATE NONCLUSTERED INDEX [IX_products_departaments] ON [dbo].[products]
(
	[department_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_products_products_types]    Script Date: 08/06/2015 19:43:07 ******/
CREATE NONCLUSTERED INDEX [IX_products_products_types] ON [dbo].[products]
(
	[product_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_produtos_similares]    Script Date: 08/06/2015 19:43:07 ******/
ALTER TABLE [dbo].[similar_products] ADD  CONSTRAINT [IX_produtos_similares] UNIQUE NONCLUSTERED 
(
	[product_id] ASC,
	[similar_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_stocks]    Script Date: 08/06/2015 19:43:07 ******/
ALTER TABLE [dbo].[stocks] ADD  CONSTRAINT [IX_stocks] UNIQUE NONCLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[bosses_departments]  WITH CHECK ADD  CONSTRAINT [FK_bosses_departments_departaments] FOREIGN KEY([department_id])
REFERENCES [dbo].[departments] ([id])
GO
ALTER TABLE [dbo].[bosses_departments] CHECK CONSTRAINT [FK_bosses_departments_departaments]
GO
ALTER TABLE [dbo].[bosses_departments]  WITH CHECK ADD  CONSTRAINT [FK_bosses_departments_sellers] FOREIGN KEY([boss_id])
REFERENCES [dbo].[sellers] ([id])
GO
ALTER TABLE [dbo].[bosses_departments] CHECK CONSTRAINT [FK_bosses_departments_sellers]
GO
ALTER TABLE [dbo].[departments_sales]  WITH CHECK ADD  CONSTRAINT [FK_departments_sales_departments] FOREIGN KEY([department_id])
REFERENCES [dbo].[departments] ([id])
GO
ALTER TABLE [dbo].[departments_sales] CHECK CONSTRAINT [FK_departments_sales_departments]
GO
ALTER TABLE [dbo].[departments_sales]  WITH CHECK ADD  CONSTRAINT [FK_departments_sales_sales] FOREIGN KEY([sale_id])
REFERENCES [dbo].[sales] ([id])
GO
ALTER TABLE [dbo].[departments_sales] CHECK CONSTRAINT [FK_departments_sales_sales]
GO
ALTER TABLE [dbo].[departments_sales]  WITH CHECK ADD  CONSTRAINT [FK_departments_sales_sellers] FOREIGN KEY([seller_id])
REFERENCES [dbo].[sellers] ([id])
GO
ALTER TABLE [dbo].[departments_sales] CHECK CONSTRAINT [FK_departments_sales_sellers]
GO
ALTER TABLE [dbo].[products]  WITH CHECK ADD  CONSTRAINT [FK_products_departments] FOREIGN KEY([department_id])
REFERENCES [dbo].[departments] ([id])
GO
ALTER TABLE [dbo].[products] CHECK CONSTRAINT [FK_products_departments]
GO
ALTER TABLE [dbo].[products]  WITH CHECK ADD  CONSTRAINT [FK_products_products_types] FOREIGN KEY([product_type_id])
REFERENCES [dbo].[products_types] ([id])
GO
ALTER TABLE [dbo].[products] CHECK CONSTRAINT [FK_products_products_types]
GO
ALTER TABLE [dbo].[products_departments_sales]  WITH CHECK ADD  CONSTRAINT [FK_products_departments_sales_departaments_sales] FOREIGN KEY([department_sale_id])
REFERENCES [dbo].[departments_sales] ([id])
GO
ALTER TABLE [dbo].[products_departments_sales] CHECK CONSTRAINT [FK_products_departments_sales_departaments_sales]
GO
ALTER TABLE [dbo].[products_departments_sales]  WITH CHECK ADD  CONSTRAINT [FK_products_departments_sales_products] FOREIGN KEY([product_id])
REFERENCES [dbo].[products] ([id])
GO
ALTER TABLE [dbo].[products_departments_sales] CHECK CONSTRAINT [FK_products_departments_sales_products]
GO
ALTER TABLE [dbo].[products_departments_sales]  WITH CHECK ADD  CONSTRAINT [FK_products_departments_sales_sales] FOREIGN KEY([sale_id])
REFERENCES [dbo].[sales] ([id])
GO
ALTER TABLE [dbo].[products_departments_sales] CHECK CONSTRAINT [FK_products_departments_sales_sales]
GO
ALTER TABLE [dbo].[sales]  WITH CHECK ADD  CONSTRAINT [FK_sales_clients] FOREIGN KEY([client_id])
REFERENCES [dbo].[clients] ([id])
GO
ALTER TABLE [dbo].[sales] CHECK CONSTRAINT [FK_sales_clients]
GO
ALTER TABLE [dbo].[sellers]  WITH CHECK ADD  CONSTRAINT [FK_sellers_departments] FOREIGN KEY([department_id])
REFERENCES [dbo].[departments] ([id])
GO
ALTER TABLE [dbo].[sellers] CHECK CONSTRAINT [FK_sellers_departments]
GO
ALTER TABLE [dbo].[similar_products]  WITH CHECK ADD  CONSTRAINT [FK_similar_products_products] FOREIGN KEY([product_id])
REFERENCES [dbo].[products] ([id])
GO
ALTER TABLE [dbo].[similar_products] CHECK CONSTRAINT [FK_similar_products_products]
GO
ALTER TABLE [dbo].[similar_products]  WITH CHECK ADD  CONSTRAINT [FK_similar_products_products1] FOREIGN KEY([similar_id])
REFERENCES [dbo].[products] ([id])
GO
ALTER TABLE [dbo].[similar_products] CHECK CONSTRAINT [FK_similar_products_products1]
GO
ALTER TABLE [dbo].[stocks]  WITH CHECK ADD  CONSTRAINT [FK_stocks_products] FOREIGN KEY([product_id])
REFERENCES [dbo].[products] ([id])
GO
ALTER TABLE [dbo].[stocks] CHECK CONSTRAINT [FK_stocks_products]
GO
USE [master]
GO
ALTER DATABASE [store] SET  READ_WRITE 
GO
