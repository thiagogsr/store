﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Store.Helpers
{
    public class UtilHelper
    {
        public static String onlyNumbers(String text)
        {
            return Regex.Replace(text, @"\D+", "");
        }

        public static DateTime parseDate(String date)
        {
            return DateTime.Parse(date);
        }

        public static String formatDate(DateTime date)
        {
            return date.ToString("dd/MM/yyyy");
        }

        public static Decimal parseDecimal(String value)
        {
            return Decimal.Parse(value);
        }

        public static byte parseByte(String value)
        {
            return Convert.ToByte(value);
        }

        public static Decimal amount(Decimal price, int quantity)
        {
            return price * quantity;
        }
    }
}