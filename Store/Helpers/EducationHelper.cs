﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Store.Helpers
{
    public class EducationHelper
    {
        public static void populate(DropDownList element)
        {
            if (element.Items.Count == 1)
            {
                Dictionary<string, string> educationList = new Dictionary<string, string>();
                educationList.Add("FUNDAMENTAL", "Ensino Fundamental");
                educationList.Add("MEDIO", "Ensino Médio");
                educationList.Add("SUPERIOR", "Ensino Superior");

                element.DataSource = educationList;
                element.AppendDataBoundItems = true;
                element.DataTextField = "Value";
                element.DataValueField = "Key";
                element.DataBind();
            }
        }
    }
}