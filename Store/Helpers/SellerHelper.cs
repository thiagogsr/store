﻿using Store.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Store.Helpers
{
    public class SellerHelper
    {
        public static void populate(DropDownList element, String department_id = null, String education = null)
        {
            if (element.Items.Count == 1)
            {
                element.DataSource = controller().index(department_id, true, education);
                element.AppendDataBoundItems = true;
                element.DataTextField = "name";
                element.DataValueField = "id";
                element.DataBind();
            }
        }

        private static SellerController controller()
        {
            return new SellerController();
        }
    }
}