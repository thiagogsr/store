﻿using Store.Controllers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Helpers
{
    public class DepartmentSaleHelper
    {
        public static DepartmentSale search(String department_sale_id)
        {
            int id = Int32.Parse(department_sale_id);
            return controller().edit(id);
        }

        private static DepartmentSaleController controller()
        {
            return new DepartmentSaleController();
        }
    }
}