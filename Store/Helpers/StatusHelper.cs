﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Store.Helpers
{
    public class StatusHelper
    {
        public static void populate(RadioButtonList element)
        {
            if (element.Items.Count == 0)
            {
                Dictionary<string, string> statuses = new Dictionary<string,string>();
                statuses.Add("1", "Ativo");
                statuses.Add("0", "Desligado");

                element.DataSource = statuses;
                element.DataTextField = "Value";
                element.DataValueField = "Key";
                element.DataBind();
            }
        }

        public static void populate_yesno(RadioButtonList element)
        {
            if (element.Items.Count == 0)
            {
                Dictionary<string, string> statuses = new Dictionary<string, string>();
                statuses.Add("1", "Sim");
                statuses.Add("0", "Não");

                element.DataSource = statuses;
                element.DataTextField = "Value";
                element.DataValueField = "Key";
                element.DataBind();
            }
        }
    }
}