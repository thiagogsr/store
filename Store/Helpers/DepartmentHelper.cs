﻿using Store.Controllers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Store.Helpers
{
    public class DepartmentHelper
    {
        public static void populate(DropDownList element, String sale_id = null)
        {
            if (element.Items.Count == 1)
            {
                element.DataSource = controller().index(sale_id);
                element.AppendDataBoundItems = true;
                element.DataTextField = "acronym";
                element.DataValueField = "id";
                element.DataBind();
            }
        }

        private static DepartmentController controller()
        {
            return new DepartmentController();
        }

        public static Department search(String departament_id)
        {
            int id = Int32.Parse(departament_id);
            return controller().edit(id);
        }
    }
}