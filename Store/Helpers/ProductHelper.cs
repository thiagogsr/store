using Store.Controllers;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Store.Helpers
{
    public class ProductHelper
    {
        public static void populate(DropDownList element, String departament_sale_id = null)
        {
            if (element.Items.Count == 1)
            {
                element.DataSource = controller().index(departament_sale_id);
                element.AppendDataBoundItems = true;
                element.DataTextField = "title";
                element.DataValueField = "id";
                element.DataBind();
            }
        }

        public static void populate_similars(DropDownList element, String product_id)
        {
            if (element.Items.Count == 1)
            {
                element.DataSource = controller().index(null, product_id);
                element.AppendDataBoundItems = true;
                element.DataTextField = "title";
                element.DataValueField = "id";
                element.DataBind();
            }
        }

        public static Product search(String product_id)
        {
            int id = Int32.Parse(product_id);
            return controller().edit(id);
        }

        private static ProductController controller()
        {
            return new ProductController();
        }
    }
}