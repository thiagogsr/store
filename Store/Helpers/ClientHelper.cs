using Store.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Store.Helpers
{
    public class ClientHelper
    {
        public static void populate(DropDownList element)
        {
            if (element.Items.Count == 1)
            {
                element.DataSource = controller().index();
                element.AppendDataBoundItems = true;
                element.DataTextField = "name";
                element.DataValueField = "id";
                element.DataBind();
            }
        }

        private static ClientController controller()
        {
            return new ClientController();
        }
    }
}