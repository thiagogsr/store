﻿<%@ Page Title="Tipo de Produtos" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Store.View.ProductType.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Tipo de Produtos</h2>
            <a href="Form.aspx" title="Novo Tipo de Produto" class="button">Novo Tipo de Produto</a>
            <asp:GridView ID="ProductTypesList" runat="server" DataKeyNames="id"
                AutoGenerateColumns="false" CssClass="table" OnRowDataBound="ProductTypesList_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="id" />
                    <asp:BoundField HeaderText="Título" DataField="title" />
                    <asp:BoundField HeaderText="Permite Similares?" DataField="allow_similar" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                OnClick="Destroy_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
