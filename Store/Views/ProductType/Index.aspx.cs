﻿using Store.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.View.ProductType
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadProductTypes();
        }

        protected void ProductTypesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Models.ProductType product_type = (Models.ProductType)e.Row.DataItem;

                if (product_type.allow_similar.Equals(0))
                {
                    e.Row.Cells[2].Text = "Não";
                }
                else
                {
                    e.Row.Cells[2].Text = "Sim";
                }

                HyperLink edit = e.Row.Cells[3].Controls[0] as HyperLink;
                edit.NavigateUrl = "~/Views/ProductType/Form.aspx?id=" + product_type.id;
            }
        }

        protected void Destroy_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton) sender).NamingContainer;
            int product_type_id = Int32.Parse(ProductTypesList.DataKeys[row.RowIndex]["id"].ToString());

            if (controller().destroy(product_type_id))
            {
                Session["flash_message"] = "Tipo de Produto removido com sucesso";
            }
            else
            {
                Session["error_message"] = "Falha ao remover o tipo de produto, tente novamente.";
            }

            loadProductTypes();
        }

        private void loadProductTypes()
        {
            ProductTypesList.DataSource = controller().index();
            ProductTypesList.DataBind();
        }

        private ProductTypeController controller()
        {
            return new ProductTypeController();
        }
    }
}