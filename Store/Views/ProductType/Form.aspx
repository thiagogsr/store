﻿<%@ Page Title="Tipo de Produto" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Store.Views.ProductType.New" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Tipo de Produto</h2>
            <asp:HiddenField ID="id" runat="server" />
            <div>
                <label>
                    <asp:Label Text="Título" runat="server" />
                    <asp:TextBox runat="server" ID="title" />
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="title" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>
                    <asp:Label Text="Permite Similares?" runat="server" />
                    <asp:RadioButtonList runat="server" ID="allow_similar"></asp:RadioButtonList>
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="allow_similar" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            </div>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Salvar" OnClick="Save_Click" UseSubmitBehavior="true" />
                <a href="Index.aspx" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
