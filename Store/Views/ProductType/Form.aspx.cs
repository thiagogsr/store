﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.ProductType
{
    public partial class New : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            populateLists();

            if (!IsPostBack)
            {
                String product_type_id = Request.QueryString["id"];

                if (product_type_id != null)
                {
                    Models.ProductType product_type = controller().edit(Int32.Parse(product_type_id));

                    if (product_type == null)
                    {
                        Session["error_message"] = "Tipo de Produto não encontrado";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        id.Value = product_type_id;
                        title.Text = product_type.title;
                        allow_similar.Text = product_type.allow_similar.ToString();
                    }
                }
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            String product_type_id = id.Value;
            bool saved;

            saved = product_type_id == "" ? create() : update();
            setMessage(saved);
        }

        private bool create()
        {
            return controller().create(title.Text, allow_similar.Text);
        }

        private bool update()
        {
            int product_type_id = Int32.Parse(id.Value);
            return controller().update(product_type_id, title.Text, allow_similar.Text);
        }

        private void setMessage(bool saved)
        {
            if (saved)
            {
                Session["flash_message"] = "Tipo de Produto salvo com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["error_message"] = "Falha ao salvar o tipo de produto, tente novamente.";
            }
        }

        private ProductTypeController controller()
        {
            return new ProductTypeController();
        }

        private void populateLists()
        {
            StatusHelper.populate_yesno(allow_similar);
        }
    }
}