﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.ProductSimilar
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String query_id = Request.QueryString["product_id"];

            if (query_id != null)
            {
                Models.Product product = ProductHelper.search(query_id);
                if (product.product_type.allow_similar == 0)
                {
                    Session["error_message"] = "Esse produto não aceita produtos similares.";
                    Response.Redirect("/Views/Product/Index.aspx");
                }

                ProductName.InnerText = product.title;

                product_id.Value = query_id;
                loadSimilars(query_id);
            }
            else
            {
                Response.Redirect("/Views/Product/Index.aspx");
            }
        }

        protected void Destroy_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
            int product = Int32.Parse(product_id.Value);
            int similar = Int32.Parse(SimilarsList.DataKeys[row.RowIndex]["id"].ToString());

            if (controller().destroy(product, similar))
            {
                Session["flash_message"] = "Produto Similar removido com sucesso";
            }
            else
            {
                Session["error_message"] = "Falha ao remover o produto similar, tente novamente.";
            }

            loadSimilars(product.ToString());
        }

        private void loadSimilars(String product_id)
        {
            SimilarsList.DataSource = controller().index(product_id);
            SimilarsList.DataBind();

            AddSimilar.HRef = "/Views/ProductSimilar/Form.aspx?product_id=" + product_id;
        }

        private ProductSimilarController controller()
        {
            return new ProductSimilarController();
        }
    }
}