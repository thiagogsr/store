﻿<%@ Page Title="Produtos Similares" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Store.Views.ProductSimilar.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Produtos Similares para <span id="ProductName" runat="server"></span></h2>
            <asp:HiddenField ID="product_id" runat="server" />
            <a href="#" id="AddSimilar" runat="server" title="Adicionar Similar" class="button">Adicionar Similar</a>

            <asp:GridView ID="SimilarsList" runat="server" DataKeyNames="id"
                AutoGenerateColumns="False" CssClass="table">
                <Columns>
                    <asp:BoundField HeaderText="Produto" DataField="title" HeaderStyle-Width="80%" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                OnClick="Destroy_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
