﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.ProductSimilar
{
    public partial class Form : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String query_id = Request.QueryString["product_id"];

            if (query_id != null)
            {
                populateLists(query_id);
                product_id.Value = query_id;

                Models.Product product = ProductHelper.search(query_id);
                ProductName.InnerText = product.title;
            }
            else
            {
                Response.Redirect("/Views/Product/Index.aspx");
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            String product = product_id.Value;
            bool saved = controller().create(product, similar_id.Text);

            if (saved)
            {
                Session["flash_message"] = "Produto Similar adicionado com sucesso";
                Response.Redirect("/Views/ProductSimilar/Index.aspx?product_id=" + product);
            }
            else
            {
                Session["error_message"] = "Falha ao adicionar o produto similar, tente novamente.";
            }
        }

        private void populateLists(String product_id)
        {
            CancelCreate.HRef = "/Views/ProductSimilar/Index.aspx?product_id=" + product_id;
            ProductHelper.populate_similars(similar_id, product_id);
        }

        private ProductSimilarController controller()
        {
            return new ProductSimilarController();
        }
    }
}