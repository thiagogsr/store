﻿<%@ Page Title="Adicionar Produto Similar" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Store.Views.ProductSimilar.Form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Adicionar Similar para <span id="ProductName" runat="server"></span></h2>
            <asp:HiddenField ID="product_id" runat="server" />
            <label>
                <asp:Label Text="Produto" runat="server" />
                <asp:DropDownList ID="similar_id" runat="server">
                    <asp:ListItem Value="">Selecione</asp:ListItem>
                </asp:DropDownList>
            </label>
            <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                ControlToValidate="similar_id" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Salvar" OnClick="Save_Click" UseSubmitBehavior="true" />
                <a href="#" runat="server" id="CancelCreate" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
