﻿<%@ Page Title="Vendas" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Store.View.Sale.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Vendas</h2>
            <a href="Form.aspx" title="Nova Venda" class="button">Nova Venda</a>
            <div class="row">
                <div class="medium-3 columns">
                    <label>
                        <asp:Label Text="Vendedor" runat="server" />
                        <asp:DropDownList ID="Seller" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                </div>
                <div class="medium-3 columns">
                    <label>
                        <asp:Label Text="Data inicial" runat="server" />
                        <asp:TextBox ID="StartDate" runat="server" CssClass="date" />
                    </label>
                </div>
                <div class="medium-3 columns">
                    <label>
                        <asp:Label Text="Data final" runat="server" />
                        <asp:TextBox ID="EndDate" runat="server" CssClass="date" />
                    </label>
                </div>
                <div class="medium-3 columns">
                    <asp:Button CssClass="button tiny search-button" runat="server" Text="Buscar" OnClick="Search_Click" UseSubmitBehavior="true" />
                </div>
            </div>
            <asp:GridView ID="SalesList" runat="server" DataKeyNames="id"
                AutoGenerateColumns="False" CssClass="table" OnRowDataBound="SalesList_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="id" />
                    <asp:BoundField HeaderText="Cliente" DataField="client.name" />
                    <asp:BoundField HeaderText="Data" DataField="sale_date" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:TemplateField HeaderText="Total">
                        <ItemTemplate>
                            R$ <asp:Label ID="Amount" runat="server" Text='<%# Bind("amount") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField Text="Departamentos da Venda" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                OnClick="Destroy_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
