﻿<%@ Page Title="Nova Venda" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Store.Views.Sale.New" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Nova Venda</h2>
            <asp:HiddenField ID="id" runat="server" />
            <div class="row">
                <div class="medium-8 columns">
                    <label>
                        <asp:Label Text="Cliente" runat="server" />
                        <asp:DropDownList ID="client" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="client" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>

                <div class="medium-4 columns">
                    <label>
                        <asp:Label Text="Data" runat="server" />
                        <asp:TextBox runat="server" ID="sale_date" CssClass="date" />
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="sale_date" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Próximo Passo" OnClick="Save_Click" UseSubmitBehavior="true" />
                <a href="Index.aspx" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
