﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Web.UI.WebControls;

namespace Store.View.Sale
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadSellers();
            loadSales();
        }

        protected void SalesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Models.Sale sale = (Models.Sale) e.Row.DataItem;

                HyperLink edit = e.Row.Cells[4].Controls[0] as HyperLink;
                edit.NavigateUrl = "~/Views/DepartmentSale/Index.aspx?sale_id=" + sale.id;
            }
        }

        protected void Destroy_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton) sender).NamingContainer;
            int sale_id = Int32.Parse(SalesList.DataKeys[row.RowIndex]["id"].ToString());

            if (controller().destroy(sale_id))
            {
                Session["flash_message"] = "Venda removida com sucesso";
            }
            else
            {
                Session["error_message"] = "Falha ao remover a venda, tente novamente.";
            }

            loadSales();
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            loadSales(Seller.Text, StartDate.Text, EndDate.Text);
        }

        private void loadSales(String seller_id = null, String start_date = null, String end_date = null)
        {
            SalesList.DataSource = controller().index(seller_id, start_date, end_date);
            SalesList.DataBind();
        }

        private void loadSellers()
        {
            SellerHelper.populate(Seller);
        }

        private SaleController controller()
        {
            return new SaleController();
        }
    }
}