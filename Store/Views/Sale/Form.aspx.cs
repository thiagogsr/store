﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.Sale
{
    public partial class New : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            populateDropDowns();

            if (!IsPostBack)
            {
                sale_date.Text = DateTime.Now.ToString();
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            int sale_id = create();

            Session["flash_message"] = "Venda salva com sucesso";
            Response.Redirect("/Views/DepartmentSale/Form.aspx?sale_id=" + sale_id);
        }

        private int create()
        {
            return controller().create(sale_date.Text, client.Text);
        }

        private SaleController controller()
        {
            return new SaleController();
        }

        private void populateDropDowns()
        {
            ClientHelper.populate(client);
        }
    }
}