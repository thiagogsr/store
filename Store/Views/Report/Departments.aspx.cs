﻿using Store.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.Report
{
    public partial class Departments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadReport();
        }

        private void loadReport()
        {
            DepartmentsList.DataSource = controller().departments();
            DepartmentsList.DataBind();
        }

        private ReportController controller()
        {
            return new ReportController();
        }
    }
}