﻿<%@ Page Title="Relatório de Departamentos" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Departments.aspx.cs" Inherits="Store.Views.Report.Departments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Relatório de Departamentos</h2>
            <asp:GridView ID="DepartmentsList" runat="server"
                AutoGenerateColumns="false" CssClass="table">
                <Columns>
                    <asp:BoundField HeaderText="Departamento" DataField="department_acronym" HeaderStyle-Width="50%" />
                    <asp:BoundField HeaderText="Mês" DataField="month" />
                    <asp:BoundField HeaderText="Ano" DataField="year" />
                    <asp:TemplateField HeaderText="Volume de vendas">
                        <ItemTemplate>
                            R$ <asp:Label ID="Amount" runat="server" Text='<%# Bind("department_amount") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
