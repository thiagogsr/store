﻿<%@ Page Title="Relatório de Vendas" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Sales.aspx.cs" Inherits="Store.Views.Report.Sales" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Relatório de Vendas</h2>
            <asp:GridView ID="SalesList" runat="server"
                AutoGenerateColumns="false" CssClass="table">
                <Columns>
                    <asp:BoundField HeaderText="Ano" DataField="year" />
                    <asp:BoundField HeaderText="Mês" DataField="month" />
                    <asp:TemplateField HeaderText="Valor vendido">
                        <ItemTemplate>
                            R$ <asp:Label ID="Amount" runat="server" Text='<%# Bind("sale_amount") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comissão paga">
                        <ItemTemplate>
                            R$ <asp:Label ID="Commission" runat="server" Text='<%# Bind("sale_commission") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
