﻿<%@ Page Title="Adicionar Departamento da Venda" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Store.Views.DepartmentSale.Form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Adicionar Departamento da Venda</h2>
            <asp:HiddenField ID="sale_id" runat="server" />
            <div>
                <label>
                    <asp:Label Text="Departamento" runat="server" />
                    <asp:DropDownList ID="department" runat="server" OnSelectedIndexChanged="department_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="">Selecione</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="department" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>
                    <asp:Label Text="Vendedor" runat="server" />
                    <asp:DropDownList ID="seller" runat="server">
                        <asp:ListItem Value="">Selecione</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="seller" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            </div>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Salvar e adicionar produtos" OnClick="Save_Click" UseSubmitBehavior="true" />
                <asp:Button CssClass="button" runat="server" Text="Salvar e adicionar outro departamento" OnClick="SaveAndContinue_Click" UseSubmitBehavior="true" />
                <a href="Index.aspx?sale_id=<%= Request.QueryString["sale_id"] %>" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
