﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.DepartmentSale
{
    public partial class Form : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                sale_id.Value = Request.QueryString["sale_id"];
                populateDropDowns(sale_id.Value);
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            Models.DepartmentSale department = create();
            setMessage();
            Response.Redirect("/Views/ProductDepartmentSale/Form.aspx?department_sale_id=" + department.id);
        }

        protected void SaveAndContinue_Click(object sender, EventArgs e)
        {
            Models.DepartmentSale department = create();
            setMessage();
            Response.Redirect("/Views/DepartmentSale/Form.aspx?sale_id=" + department.sale_id);
        }

        protected void department_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList department = (DropDownList) sender;
            SellerHelper.populate(seller, department.SelectedValue);
        }

        private Models.DepartmentSale create()
        {
            return controller().create(sale_id.Value, department.Text, seller.Text);
        }

        private void setMessage()
        {
            Session["flash_message"] = "Departamento da venda salvo com sucesso";
        }

        private void populateDropDowns(String sale_id)
        {
            DepartmentHelper.populate(department, sale_id);
        }

        private DepartmentSaleController controller()
        {
            return new DepartmentSaleController();
        }
    }
}