﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Web.UI.WebControls;

namespace Store.Views.DepartmentSale
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadDepartmentsSales();
        }

        protected void DepartmentsSalesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Models.DepartmentSale department_sale = (Models.DepartmentSale)e.Row.DataItem;

                HyperLink products_link = e.Row.Cells[3].Controls[0] as HyperLink;
                products_link.NavigateUrl = "/Views/ProductDepartmentSale/Index.aspx?department_sale_id=" + department_sale.id;
            }
        }

        protected void Destroy_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow) ((LinkButton)sender).NamingContainer;
            int department_sale_id = Int32.Parse(DepartmentsSalesList.DataKeys[row.RowIndex]["id"].ToString());

            if (controller().destroy(department_sale_id))
            {
                Session["flash_message"] = "Departamento da venda removido com sucesso";
            }
            else
            {
                Session["error_message"] = "Falha ao remover o departamento da venda, tente novamente.";
            }

            loadDepartmentsSales();
        }

        private void loadDepartmentsSales()
        {
            int sale_id = Int32.Parse(Request.QueryString["sale_id"]);
            Models.Sale sale = sale_controller().edit(sale_id);

            DepartmentsSalesList.DataSource = controller().index_by(sale_id);
            DepartmentsSalesList.DataBind();

            SaleClient.InnerText = sale.client.name;
            SaleDate.InnerText = UtilHelper.formatDate(sale.sale_date);
            SaleAmount.InnerText = sale.amount.ToString();
        }

        private DepartmentSaleController controller()
        {
            return new DepartmentSaleController();
        }

        private SaleController sale_controller()
        {
            return new SaleController();
        }
    }
}