﻿<%@ Page Title="Departamentos da Venda" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Store.Views.DepartmentSale.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Departamentos da Venda</h2>
            <p><strong>Cliente:</strong> <span runat="server" id="SaleClient"></span></p>
            <p><strong>Data:</strong> <span runat="server" id="SaleDate"></span></p>
            <p><strong>Total da Venda:</strong> R$ <span runat="server" id="SaleAmount"></span></p>

            <a href="Form.aspx?sale_id=<%= Request.QueryString["sale_id"] %>" title="Adicionar Departamento" class="button">Adicionar Departamento</a>

            <asp:GridView ID="DepartmentsSalesList" runat="server" DataKeyNames="id"
                AutoGenerateColumns="False" CssClass="table" OnRowDataBound="DepartmentsSalesList_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="Departamento" DataField="department.acronym" HeaderStyle-Width="30%" />
                    <asp:BoundField HeaderText="Vendedor" DataField="seller.name" HeaderStyle-Width="30%" />
                    <asp:TemplateField HeaderText="Comissão">
                        <ItemTemplate>
                            <asp:Label ID="commission" runat="server" Text='<%# Bind("commission") %>'></asp:Label>%
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField Text="Produtos" />
                    <asp:TemplateField HeaderText="Valor Total">
                        <ItemTemplate>
                            R$ <asp:Label ID="amount" runat="server" Text='<%# Bind("amount") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                OnClick="Destroy_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
