﻿<%@ Page Title="Vendedor" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Store.Views.Seller.New" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Vendedor</h2>
            <asp:HiddenField ID="id" runat="server" />
            <div>
                <label>
                    <asp:Label Text="Nome" runat="server" />
                    <asp:TextBox runat="server" ID="name" />
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="name" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            </div>
            <div class="row">
                <div class="medium-6 columns">
                    <label>
                        <asp:Label Text="RG" runat="server" />
                        <asp:TextBox runat="server" ID="rg" />
                    </label>
                </div>

                <div class="medium-6 columns">
                    <label>
                        <asp:Label Text="CPF" runat="server" />
                        <asp:TextBox runat="server" ID="cpf" CssClass="cpf" />
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="medium-6 columns">
                    <label>
                        <asp:Label Text="Nível de Escolaridade" runat="server" />
                        <asp:DropDownList ID="education" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="education" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>

                <div class="medium-6 columns">
                    <label>
                        <asp:Label Text="Admissão" runat="server" />
                        <asp:TextBox runat="server" ID="admission" CssClass="date" />
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="admission" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="medium-6 columns">
                    <label>
                        <asp:Label Text="Departamento" runat="server" />
                        <asp:DropDownList ID="department" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="department" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>

                <div class="medium-6 columns">
                    <label>
                        <asp:Label Text="Situação" runat="server" />
                        <asp:RadioButtonList runat="server" ID="status"></asp:RadioButtonList>
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="status" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Salvar" OnClick="Save_Click" UseSubmitBehavior="true" />
                <a href="Index.aspx" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
