﻿<%@ Page Title="Vendedores" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Store.View.Seller.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Vendedores</h2>
            <a href="Form.aspx" title="Novo Vendedor" class="button">Novo Vendedor</a>
            <asp:GridView ID="SellersList" runat="server" DataKeyNames="id"
                AutoGenerateColumns="false" CssClass="table" OnRowDataBound="SellersList_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="id" />
                    <asp:BoundField HeaderText="Nome" DataField="name" />
                    <asp:BoundField HeaderText="RG" DataField="rg" />
                    <asp:BoundField HeaderText="CPF" DataField="cpf" />
                    <asp:BoundField HeaderText="Admissão" DataField="admission" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField HeaderText="Situação" DataField="status" />
                    <asp:BoundField HeaderText="Departamento" DataField="department.acronym" />
                    <asp:HyperLinkField HeaderText="Chefe" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                OnClick="Destroy_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
