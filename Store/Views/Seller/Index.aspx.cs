﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.View.Seller
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadSellers();
        }

        protected void SellersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Models.Seller seller = (Models.Seller) e.Row.DataItem;

                if (seller.status.Equals(0))
                {
                    e.Row.Cells[5].Text = "Desligado";
                }
                else
                {
                    e.Row.Cells[5].Text = "Ativo";
                }

                Models.BossDepartment boss_department = controller().current_boss_department(seller);
                HyperLink boss = e.Row.Cells[7].Controls[0] as HyperLink;

                if (boss_department == null)
                {
                    boss.Text = "Indisponível";
                    boss.CssClass = "unavailable";
                }
                else
                {
                    boss.NavigateUrl = "~/Views/BossDepartment/Index.aspx?department_id=" + boss_department.department_id;
                    boss.Text = boss_department.department.acronym + " - Desde " + UtilHelper.formatDate(boss_department.start_date);
                }

                HyperLink edit = e.Row.Cells[8].Controls[0] as HyperLink;
                edit.NavigateUrl = "~/Views/Seller/Form.aspx?id=" + seller.id;
            }
        }

        protected void Destroy_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton) sender).NamingContainer;
            int seller_id = Int32.Parse(SellersList.DataKeys[row.RowIndex]["id"].ToString());

            if (controller().destroy(seller_id))
            {
                Session["flash_message"] = "Vendedor removido com sucesso";
            }
            else
            {
                Session["error_message"] = "Falha ao remover o vendedor, tente novamente.";
            }

            loadSellers();
        }

        private void loadSellers()
        {
            SellersList.DataSource = controller().index();
            SellersList.DataBind();
        }

        private SellerController controller()
        {
            return new SellerController();
        }
    }
}