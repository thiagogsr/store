﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.Seller
{
    public partial class New : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            populateLists();

            if (!IsPostBack)
            {
                String seller_id = Request.QueryString["id"];

                if (seller_id != null)
                {
                    Models.Seller seller = controller().edit(Int32.Parse(seller_id));

                    if (seller == null)
                    {
                        Session["error_message"] = "Vendedor não encontrado";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        id.Value = seller_id;
                        name.Text = seller.name;
                        rg.Text = seller.rg;
                        cpf.Text = seller.cpf;
                        education.Text = seller.education;
                        admission.Text = seller.admission.ToString();
                        status.Text = seller.status.ToString();
                        department.Text = seller.department_id.ToString();
                    }
                }
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            String seller_id = id.Value;
            bool saved;

            saved = seller_id == "" ? create() : update();
            setMessage(saved);
        }

        private bool create()
        {
            return controller().create(name.Text, rg.Text, cpf.Text, education.Text,
                admission.Text, status.Text, department.Text);
        }

        private bool update()
        {
            int seller_id = Int32.Parse(id.Value);
            return controller().update(seller_id, name.Text, rg.Text, cpf.Text,
                education.Text, admission.Text, status.Text, department.Text);
        }

        private void setMessage(bool saved)
        {
            if (saved)
            {
                Session["flash_message"] = "Vendedor salvo com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["error_message"] = "Falha ao salvar o vendedor, tente novamente.";
            }
        }

        private SellerController controller()
        {
            return new SellerController();
        }

        private void populateLists()
        {
            EducationHelper.populate(education);
            DepartmentHelper.populate(department);
            StatusHelper.populate(status);
        }
    }
}