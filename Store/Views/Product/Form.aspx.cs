﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.Product
{
    public partial class New : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            populateDropDowns();

            if (!IsPostBack)
            {
                String product_id = Request.QueryString["id"];

                if (product_id != null)
                {
                    Models.Product product = controller().edit(Int32.Parse(product_id));

                    if (product == null)
                    {
                        Session["error_message"] = "Produto não encontrado";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        id.Value = product_id;
                        title.Text = product.title;
                        price.Text = product.price.ToString();
                        department.Text = product.department_id.ToString();
                        product_type.Text = product.product_type_id.ToString();
                    }
                }
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            String product_id = id.Value;
            bool saved = product_id == "" ? create() : update();
            setMessage(saved);
        }

        private bool create()
        {
            return controller().create(title.Text, price.Text, department.Text, product_type.Text);
        }

        private bool update()
        {
            int product_id = Int32.Parse(id.Value);
            return controller().update(product_id, title.Text, price.Text, department.Text, product_type.Text);
        }

        private void setMessage(bool saved)
        {
            if (saved)
            {
                Session["flash_message"] = "Produto salvo com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["error_message"] = "Falha ao salvar o produto, tente novamente.";
            }
        }

        private ProductController controller()
        {
            return new ProductController();
        }

        private void populateDropDowns()
        {
            DepartmentHelper.populate(department);
            ProductTypeHelper.populate(product_type);
        }
    }
}