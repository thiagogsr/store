﻿using Store.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.View.Product
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadProducts();
        }

        protected void ProductsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Models.Product product = (Models.Product) e.Row.DataItem;

                HyperLink buy = e.Row.Cells[6].Controls[0] as HyperLink;
                buy.NavigateUrl = "~/Views/Product/Buy.aspx?product_id=" + product.id;

                HyperLink similar = e.Row.Cells[7].Controls[0] as HyperLink;
                if (product.product_type.allow_similar == 1)
                {
                    similar.NavigateUrl = "~/Views/ProductSimilar/Index.aspx?product_id=" + product.id;
                    similar.Text = product.similars.Count + " Similare(s)";
                }
                else
                {
                    similar.Text = "Indisponível";
                    similar.CssClass = "unavailable";
                }

                HyperLink edit = e.Row.Cells[8].Controls[0] as HyperLink;
                edit.NavigateUrl = "~/Views/Product/Form.aspx?id=" + product.id;
            }
        }

        protected void Destroy_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton) sender).NamingContainer;
            int product_id = Int32.Parse(ProductsList.DataKeys[row.RowIndex]["id"].ToString());

            if (controller().destroy(product_id))
            {
                Session["flash_message"] = "Produto removido com sucesso";
            }
            else
            {
                Session["error_message"] = "Falha ao remover o produto, tente novamente.";
            }

            loadProducts();
        }

        private void loadProducts()
        {
            String department_id = Request.QueryString["department_id"];

            if (department_id == null)
            {
                ProductsList.DataSource = controller().index();
            }
            else
            {
                ProductsList.DataSource = controller().index_by(Int32.Parse(department_id));
            }
            ProductsList.DataBind();
        }

        private ProductController controller()
        {
            return new ProductController();
        }
    }
}