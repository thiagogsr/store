﻿<%@ Page Title="Produtos" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Store.View.Product.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Produtos</h2>
            <a href="Form.aspx" title="Novo Produto" class="button">Novo Produto</a>
            <asp:GridView ID="ProductsList" runat="server" DataKeyNames="id"
                AutoGenerateColumns="False" CssClass="table" OnRowDataBound="ProductsList_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="id" />
                    <asp:BoundField HeaderText="Título" DataField="title" />
                    <asp:TemplateField HeaderText="Preço">
                        <ItemTemplate>
                            R$ <asp:Label ID="ProductPrice" runat="server" Text='<%# Bind("price") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Departamento" DataField="department.acronym" />
                    <asp:BoundField HeaderText="Tipo" DataField="product_type.title" />
                    <asp:BoundField HeaderText="Estoque" DataField="stock.quantity" />
                    <asp:HyperLinkField Text="Comprar" />
                    <asp:HyperLinkField Text="Similares" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                OnClick="Destroy_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
