﻿<%@ Page Title="Produto" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Store.Views.Product.New" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Produto</h2>
            <asp:HiddenField ID="id" runat="server" />
            <div class="row">
                <div class="medium-8 columns">
                    <label>
                        <asp:Label Text="Título" runat="server" />
                        <asp:TextBox runat="server" ID="title" />
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="title" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>

                <div class="medium-4 columns">
                    <label>
                        <asp:Label Text="Preço" runat="server" />
                        <asp:TextBox runat="server" ID="price" CssClass="price" />
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="price" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="medium-6 columns">
                    <label>
                        <asp:Label Text="Departamento" runat="server" />
                        <asp:DropDownList ID="department" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="department" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>

                <div class="medium-6 columns">
                    <label>
                        <asp:Label Text="Tipo de Produto" runat="server" />
                        <asp:DropDownList ID="product_type" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="product_type" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Salvar" OnClick="Save_Click" UseSubmitBehavior="true" />
                <a href="Index.aspx" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
