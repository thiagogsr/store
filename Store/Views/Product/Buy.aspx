﻿<%@ Page Title="Comprar Produto" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Buy.aspx.cs" Inherits="Store.Views.Product.Buy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Comprar <span id="ProcuctName" runat="server"></span></h2>
            <asp:HiddenField ID="product_id_field" runat="server" />
            <div class="row">
                <div class="medium-6 columns">
                    <label>
                        <asp:Label Text="Quantidade" runat="server" />
                        <asp:TextBox runat="server" ID="quantity" />
                    </label>
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                        ControlToValidate="quantity" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Comprar" OnClick="Buy_Click" UseSubmitBehavior="true" />
                <a href="Index.aspx" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
