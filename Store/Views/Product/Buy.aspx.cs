﻿using Store.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.Product
{
    public partial class Buy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String product_id = Request.QueryString["product_id"];

                if (product_id != null)
                {
                    Models.Product product = controller().edit(Int32.Parse(product_id));

                    if (product == null)
                    {
                        Session["error_message"] = "Produto não encontrado";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        product_id_field.Value = product_id;
                        ProcuctName.InnerText = product.title;
                    }
                }
            }
        }

        protected void Buy_Click(object sender, EventArgs e)
        {
            int product_id = Int32.Parse(product_id_field.Value);
            int product_quantity = Int32.Parse(quantity.Text);

            stock_controller().increase(product_id, product_quantity);

            Session["flash_message"] = "Compra do produto realizada com sucesso.";
            Response.Redirect("Index.aspx");
        }

        private StockController stock_controller()
        {
            return new StockController();
        }

        private ProductController controller()
        {
            return new ProductController();
        }
    }
}