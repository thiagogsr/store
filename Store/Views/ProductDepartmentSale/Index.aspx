﻿<%@ Page Title="Produtos do Departamento da Venda" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Store.Views.ProductDepartmentSale.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Produtos do Departamento da Venda</h2>
            <p><strong>Cliente:</strong> <span runat="server" id="SaleClient"></span></p>
            <p><strong>Data:</strong> <span runat="server" id="SaleDate"></span></p>
            <p><strong>Departamento:</strong> <span runat="server" id="Department"></span></p>
            <p><strong>Vendedor:</strong> <span runat="server" id="Seller"></span></p>
            <p><strong>Total do Departamento:</strong> R$ <span runat="server" id="DepartamentSaleAmount"></span></p>
            <p><strong>Total da Venda:</strong> R$ <span runat="server" id="SaleAmount"></span></p>

            <a href="Form.aspx?department_sale_id=<%= Request.QueryString["department_sale_id"] %>" title="Adicionar Produto" class="button">Adicionar Produto</a>

            <asp:GridView ID="ProductsDepartmentsSalesList" runat="server" DataKeyNames="id"
                AutoGenerateColumns="False" CssClass="table">
                <Columns>
                    <asp:BoundField HeaderText="Produto" DataField="product.title" HeaderStyle-Width="50%" />
                    <asp:TemplateField HeaderText="Preço">
                        <ItemTemplate>
                            R$ <asp:Label ID="Price" runat="server" Text='<%# Bind("price") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Quantidade" DataField="quantity" />
                    <asp:TemplateField HeaderText="Total">
                        <ItemTemplate>
                            R$ <asp:Label ID="Amount" runat="server" Text='<%# Bind("amount") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                OnClick="Destroy_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
