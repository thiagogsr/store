﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.ProductDepartmentSale
{
    public partial class Form : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                department_sale_id.Value = Request.QueryString["department_sale_id"];
                populateDropDowns(department_sale_id.Value);
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            Models.ProductDepartmentSale product_department_sale = create();

            if (product_department_sale != null)
            {
                Response.Redirect("Index.aspx?department_sale_id=" + product_department_sale.department_sale_id);
            }
        }

        protected void SaveAndContinue_Click(object sender, EventArgs e)
        {
            Models.ProductDepartmentSale product_department_sale = create();

            if (product_department_sale != null)
            {
                Response.Redirect("Form.aspx?department_sale_id=" + product_department_sale.department_sale_id);
            }
        }

        private Models.ProductDepartmentSale create()
        {
            Models.ProductDepartmentSale pds = controller().create(department_sale_id.Value, product.Text, quantity.Text);
        
            if (pds == null)
            {
                setMessage(false);
                return null;
            }

            setMessage();
            return pds;
        }

        private void setMessage(bool status = true)
        {
            if (status)
            {
                Session["flash_message"] = "Produto do Departamento da venda salvo com sucesso";
            }
            else
            {
                Session["error_message"] = "Não há quantidade suficiente desse produto no estoque";
            }
        }

        private void populateDropDowns(String department_sale_id)
        {
            ProductHelper.populate(product, department_sale_id);
        }

        private ProductDepartmentSaleController controller()
        {
            return new ProductDepartmentSaleController();
        }
    }
}