﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.ProductDepartmentSale
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadProductsDepartmentsSales();
        }

        protected void Destroy_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
            int product_department_sale_id = Int32.Parse(ProductsDepartmentsSalesList.DataKeys[row.RowIndex]["id"].ToString());

            if (controller().destroy(product_department_sale_id))
            {
                Session["flash_message"] = "Produto do Departamento da venda removido com sucesso";
            }
            else
            {
                Session["error_message"] = "Falha ao remover o produto do departamento da venda, tente novamente.";
            }

            loadProductsDepartmentsSales();
        }

        private void loadProductsDepartmentsSales()
        {
            int department_sale_id = Int32.Parse(Request.QueryString["department_sale_id"]);
            Models.DepartmentSale department_sale = department_sale_controller().edit(department_sale_id);
            Models.Sale sale = sale_controller().edit(department_sale.sale_id);

            ProductsDepartmentsSalesList.DataSource = controller().index_by(department_sale_id);
            ProductsDepartmentsSalesList.DataBind();

            SaleClient.InnerText = sale.client.name;
            Department.InnerText = department_sale.department.acronym;
            Seller.InnerText = department_sale.seller.name;
            SaleDate.InnerText = UtilHelper.formatDate(sale.sale_date);
            SaleAmount.InnerText = sale.amount.ToString();
            DepartamentSaleAmount.InnerText = department_sale.amount.ToString();
        }

        private ProductDepartmentSaleController controller()
        {
            return new ProductDepartmentSaleController();
        }

        private DepartmentSaleController department_sale_controller()
        {
            return new DepartmentSaleController();
        }

        private SaleController sale_controller()
        {
            return new SaleController();
        }
    }
}