﻿<%@ Page Title="Adicionar producto ao departamento da venda" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Store.Views.ProductDepartmentSale.Form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Adicionar Produto ao Departamento da Venda</h2>
            <asp:HiddenField ID="department_sale_id" runat="server" />
            <div>
                <label>
                    <asp:Label Text="Produto" runat="server" />
                    <asp:DropDownList ID="product" runat="server">
                        <asp:ListItem Value="">Selecione</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="product" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>
                    <asp:Label Text="Quantidade" runat="server" />
                    <asp:TextBox TextMode="Number" runat="server" ID="quantity" />
                    <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="quantity" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                </label>
            </div>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Salvar e adicionar outro produto" OnClick="SaveAndContinue_Click" UseSubmitBehavior="true" />
                <asp:Button CssClass="button" runat="server" Text="Salvar e voltar para lista de produtos" OnClick="Save_Click" UseSubmitBehavior="true" />
                <a href="Index.aspx?department_sale_id=<%= Request.QueryString["department_sale_id"] %>" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
