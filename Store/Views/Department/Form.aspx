﻿<%@ Page Title="Departamento" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Store.Views.Department.New" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Departamento</h2>
            <asp:HiddenField ID="id" runat="server" />
            <div>
                <label>
                    <asp:Label Text="Sigla" runat="server" />
                    <asp:TextBox runat="server" ID="acronym" />
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="acronym" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>
                    <asp:Label Text="Comissão (%)" runat="server" />
                    <asp:TextBox runat="server" ID="commission" CssClass="price" />
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="commission" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
                <asp:RangeValidator CssClass="error-message" runat="server" MinimumValue="0" MaximumValue="100,00" Type="Currency"
                    ControlToValidate="commission" ErrorMessage="Comissão não pode ser maior que 100%" EnableClientScript="true"></asp:RangeValidator>
            </div>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Salvar" OnClick="Save_Click" UseSubmitBehavior="true" />
                <a href="Index.aspx" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
