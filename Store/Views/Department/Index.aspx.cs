﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Web.UI.WebControls;

namespace Store.View.Department
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadDepartaments();
        }

        protected void DepartmentsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Models.Department department = (Models.Department) e.Row.DataItem;
                Models.BossDepartment current_boss = controller().current_boss(department);

                HyperLink boss = e.Row.Cells[3].Controls[0] as HyperLink;
                if (current_boss == null)
                {
                    boss.Text = "Indisponível";
                    boss.CssClass = "unavailable";
                }
                else
                {
                    boss.NavigateUrl = "~/Views/Seller/Form.aspx?id=" + current_boss.boss_id;
                    boss.Text = current_boss.seller.name + " - Desde " + UtilHelper.formatDate(current_boss.start_date);
                }

                HyperLink edit = e.Row.Cells[4].Controls[0] as HyperLink;
                edit.NavigateUrl = "~/Views/Department/Form.aspx?id=" + department.id;

                HyperLink bosses = e.Row.Cells[5].Controls[0] as HyperLink;
                bosses.NavigateUrl = "~/Views/BossDepartment/Index.aspx?department_id=" + department.id;

                HyperLink products = e.Row.Cells[6].Controls[0] as HyperLink;
                products.NavigateUrl = "~/Views/Product/Index.aspx?department_id=" + department.id;
            }
        }

        protected void Destroy_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton) sender).NamingContainer;
            int department_id = Int32.Parse(DepartmentsList.DataKeys[row.RowIndex]["id"].ToString());

            if (controller().destroy(department_id))
            {
                Session["flash_message"] = "Departamento removido com sucesso";
            }
            else
            {
                Session["error_message"] = "Falha ao remover o departamento, tente novamente.";
            }

            loadDepartaments();
        }

        private void loadDepartaments()
        {
            DepartmentsList.DataSource = controller().index();
            DepartmentsList.DataBind();
        }

        private DepartmentController controller()
        {
            return new DepartmentController();
        }
    }
}