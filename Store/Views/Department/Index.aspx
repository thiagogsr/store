﻿<%@ Page Title="Departamentos" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Store.View.Department.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Departamentos</h2>
            <a href="Form.aspx" title="Novo Departamento" class="button">Novo Departamento</a>
            <asp:GridView ID="DepartmentsList" runat="server" DataKeyNames="id"
                AutoGenerateColumns="False" CssClass="table" OnRowDataBound="DepartmentsList_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="id" />
                    <asp:BoundField HeaderText="Sigla" DataField="acronym" />
                    <asp:TemplateField HeaderText="Comissão">
                        <ItemTemplate>
                            <asp:Label ID="commission" runat="server" Text='<%# Bind("commission") %>'></asp:Label>%
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField HeaderText="Chefe Atual" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:HyperLinkField Text="Chefes" />
                    <asp:HyperLinkField Text="Produtos" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                OnClick="Destroy_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
