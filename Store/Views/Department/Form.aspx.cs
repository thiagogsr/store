﻿using Store.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.Department
{
    public partial class New : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String department_id = Request.QueryString["id"];

                if (department_id != null)
                {
                    Models.Department department = controller().edit(Int32.Parse(department_id));

                    if (department == null)
                    {
                        Session["error_message"] = "Departamento não encontrado";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        id.Value = department_id;
                        acronym.Text = department.acronym;
                        commission.Text = department.commission.ToString();
                    }
                }
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            String department_id = id.Value;
            bool saved;

            saved = department_id == "" ? create() : update();
            setMessage(saved);
        }

        private bool create()
        {
            return controller().create(acronym.Text, commission.Text);
        }

        private bool update()
        {
            int department_id = Int32.Parse(id.Value);
            return controller().update(department_id, acronym.Text, commission.Text);
        }

        private void setMessage(bool saved)
        {
            if (saved)
            {
                Session["flash_message"] = "Departamento salvo com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["error_message"] = "Falha ao salvar o departmento, tente novamente.";
            }
        }

        private DepartmentController controller()
        {
            return new DepartmentController();
        }
    }
}