﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.BossDepartment
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String department_id = Request.QueryString["department_id"];

            if (department_id == null)
            {
                Session["error_message"] = "Departamento não encontrado";
                Response.Redirect("/Views/Department/Index.aspx");
            }
            else
            {
                loadBosses(department_id);
            }
        }

        protected void Destroy_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
            int boss_department_id = Int32.Parse(BossesDepartmentsList.DataKeys[row.RowIndex]["id"].ToString());

            if (controller().destroy(boss_department_id))
            {
                Session["flash_message"] = "Chefe removido com sucesso";
            }
            else
            {
                Session["error_message"] = "Falha ao remover o cliente, tente novamente.";
            }

            loadBosses(Request.QueryString["department_id"]);
        }

        protected void BossesDepartmentsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Models.BossDepartment boss_department = (Models.BossDepartment) e.Row.DataItem;

                HyperLink edit = e.Row.Cells[3].Controls[0] as HyperLink;
                edit.NavigateUrl = "~/Views/BossDepartment/Form.aspx?id=" + boss_department.id + "&department_id=" + boss_department.department_id;
            }
        }

        private void loadBosses(String department_id)
        {
            int id = Int32.Parse(department_id);

            Models.Department department = department_controller().edit(id);
            DepartmentName.InnerText = department.acronym;

            BossesDepartmentsList.DataSource = controller().index(department_id);
            BossesDepartmentsList.DataBind();
        }

        private BossDepartmentController controller()
        {
            return new BossDepartmentController();
        }

        private DepartmentController department_controller()
        {
            return new DepartmentController();
        }
    }
}