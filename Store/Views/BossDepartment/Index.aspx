﻿<%@ Page Title="Chefes do Departamento" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Store.Views.BossDepartment.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Chefes do departamento <span runat="server" id="DepartmentName"></span></h2>

            <a href="Form.aspx?department_id=<%= Request.QueryString["department_id"] %>" title="Adicionar Chefe" class="button">Adicionar Chefe</a>

            <asp:GridView ID="BossesDepartmentsList" runat="server" DataKeyNames="id"
                AutoGenerateColumns="False" CssClass="table" OnRowDataBound="BossesDepartmentsList_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="Vendedor" DataField="seller.name" HeaderStyle-Width="50%" />
                    <asp:BoundField HeaderText="Data de início" DataField="start_date" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField HeaderText="Data de término" DataField="end_date" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                OnClick="Destroy_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
