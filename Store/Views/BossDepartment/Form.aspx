﻿<%@ Page Title="Adicionar chefe de departamento" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Store.Views.BossDepartment.Form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Adicionar chefe de departamento</h2>
            <asp:HiddenField ID="id" runat="server" />
            <asp:HiddenField ID="department_id" runat="server" />
            <div>
                <label>
                    <asp:Label Text="Vendedor" runat="server" />
                    <asp:DropDownList ID="seller_id" runat="server">
                        <asp:ListItem Value="">Selecione</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="seller_id" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>
                    <asp:Label Text="Início" runat="server" />
                    <asp:TextBox runat="server" ID="start_date" CssClass="date" />
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="start_date" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>
                    <asp:Label Text="Término" runat="server" />
                    <asp:TextBox runat="server" ID="end_date" CssClass="date" />
                </label>
            </div>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Salvar" OnClick="Save_Click" UseSubmitBehavior="true" />
                <a href="Index.aspx?department_id=<%= Request.QueryString["department_id"] %>" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
