﻿using Store.Controllers;
using Store.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.BossDepartment
{
    public partial class Form : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String dep_id = Request.QueryString["department_id"];

            populateDropDowns(dep_id);
            department_id.Value = dep_id;

            if (!IsPostBack)
            {
                String boss_department_id = Request.QueryString["id"];

                if (boss_department_id != null)
                {
                    Models.BossDepartment boss_department = controller().edit(boss_department_id);

                    if (boss_department != null)
                    {
                        id.Value = boss_department.id.ToString();
                        seller_id.Text = boss_department.boss_id.ToString();
                        start_date.Text = boss_department.start_date.ToString();
                        end_date.Text = boss_department.end_date.ToString();
                    }
                    else
                    {
                        Session["error_message"] = "Chefe de departamento não encontrado";
                        Response.Redirect("/Views/BossDepartment/Index.aspx?department_id=" + dep_id);
                    }
                }
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            int department_id = id.Value == "" ? create() : update();

            if (department_id == 0)
            {
                Session["error_message"] = "A data de início deve ser menor ou igual a data de término";
            }
            else if (department_id == -1)
            {
                Session["error_message"] = "Já existe um chefe ativo nesse período";
            }
            else if (department_id == -2)
            {
                Session["error_message"] = "Só deve existir um registro sem data fim para esse departamento";
            }
            else
            {
                Session["flash_message"] = "Chefe salvo com sucesso";
                Response.Redirect("/Views/BossDepartment/Index.aspx?department_id=" + department_id);
            }
        }

        private int create()
        {
            return controller().create(department_id.Value, seller_id.Text, start_date.Text, end_date.Text);
        }

        private int update()
        {
            return controller().update(id.Value, seller_id.Text, start_date.Text, end_date.Text);
        }

        private BossDepartmentController controller()
        {
            return new BossDepartmentController();
        }

        private void populateDropDowns(String department_id)
        {
            SellerHelper.populate(seller_id, department_id, "SUPERIOR");
        }
    }
}