﻿<%@ Page Title="Clientes" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Store.View.Client.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Clientes</h2>
            <a href="Form.aspx" title="Novo Cliente" class="button">Novo Cliente</a>
            <asp:GridView ID="ClientsList" runat="server" DataKeyNames="id"
                AutoGenerateColumns="false" CssClass="table" OnRowDataBound="ClientsList_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="id" />
                    <asp:BoundField HeaderText="Nome" DataField="name" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                OnClick="Destroy_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
