﻿using Store.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.Views.Client
{
    public partial class New : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String client_id = Request.QueryString["id"];

                if (client_id != null)
                {
                    Models.Client client = controller().edit(Int32.Parse(client_id));

                    if (client == null)
                    {
                        Session["error_message"] = "Cliente não encontrado";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        id.Value = client_id;
                        name.Text = client.name;
                    }
                }
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            String client_id = id.Value;
            bool saved;

            saved = client_id == "" ? create() : update();
            setMessage(saved);
        }

        private bool create()
        {
            return controller().create(name.Text);
        }

        private bool update()
        {
            int client_id = Int32.Parse(id.Value);
            return controller().update(client_id, name.Text);
        }

        private void setMessage(bool saved)
        {
            if (saved)
            {
                Session["flash_message"] = "Cliente salvo com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["error_message"] = "Falha ao salvar o cliente, tente novamente.";
            }
        }

        private ClientController controller()
        {
            return new ClientController();
        }
    }
}