﻿using Store.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Store.View.Client
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadClients();
        }

        protected void ClientsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Models.Client client = (Models.Client)e.Row.DataItem;

                HyperLink edit = e.Row.Cells[2].Controls[0] as HyperLink;
                edit.NavigateUrl = "~/Views/Client/Form.aspx?id=" + client.id;
            }
        }

        protected void Destroy_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton) sender).NamingContainer;
            int client_id = Int32.Parse(ClientsList.DataKeys[row.RowIndex]["id"].ToString());

            if (controller().destroy(client_id))
            {
                Session["flash_message"] = "Cliente removido com sucesso";
            }
            else
            {
                Session["error_message"] = "Falha ao remover o cliente, tente novamente.";
            }

            loadClients();
        }

        private void loadClients()
        {
            ClientsList.DataSource = controller().index();
            ClientsList.DataBind();
        }

        private ClientController controller()
        {
            return new ClientController();
        }
    }
}