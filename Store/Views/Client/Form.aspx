﻿<%@ Page Title="Cliente" Language="C#" MasterPageFile="~/Views/Layout/Application.Master" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Store.Views.Client.New" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="medium-12 columns">
            <h2>Cliente</h2>
            <asp:HiddenField ID="id" runat="server" />
            <div>
                <label>
                    <asp:Label Text="Nome" runat="server" />
                    <asp:TextBox runat="server" ID="name" />
                </label>
                <asp:RequiredFieldValidator CssClass="error-message" runat="server"
                    ControlToValidate="name" ErrorMessage="Campo obrigatório" EnableClientScript="true"></asp:RequiredFieldValidator>
            </div>
            <div>
                <asp:Button CssClass="button" runat="server" Text="Salvar" OnClick="Save_Click" UseSubmitBehavior="true" />
                <a href="Index.aspx" title="Cancelar" class="button info">Cancelar</a>
            </div>
        </div>
    </div>
</asp:Content>
